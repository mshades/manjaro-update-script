#!/bin/bash 

# installation instructions:
# make executable: $ chmod +x pacman.sh
# update .bashrc: $ nano .bashrc
# export update="pacman.sh"
# source .bashrc
# mkdir .tempaur
# usage: $update

# optimize mirrors
sudo pacman-mirrors -f 3

# update keyring
sudo pacman -Sy archlinux-keyring manjaro-keyring
sudo pacman-key --init

# sudo pacman-key --populate archlinux  # Updates pacman keys
# sudo pacman-key --populate manjaro    # Updates pacman keys
sudo pacman-key --populate archlinux manjaro
sudo pacman-key --refresh-keys  

# update full system
sudo pacman -Syyu

# optimize database (dont use this command on ssd)
sudo pacman-optimize && sync

# remove unused packages (orphans)
sudo pacman -Rsn $(pacman -Qdtq)
# If you get this error, don't worry: it means you don't have orphaned packages to remove! 
# error: no targets specified (use -h for help)

# safely remove old packages
sudo paccache -rvk3

# remove all the cached packages that are not currently installed, and the unused sync database
sudo pacman -Sc

# upgrade pip
pip install --upgrade pip

# upgrade yaourt packages 
# sudo yaourt -Syua
# yaourt -Syu --aur
# yaourt -Syua

# please not that yaourt not on development, yay is in current release. 

# update aur packages with extra space, for flightgear for example
# rm -rf .tempaur
# mkdir .tempaur
yaourt -Syua --tmp ~/.tempaur

# remove yaourt orphaned packages
yaourt -Qdt

# remove pacman lock ( if you need )
# sudo rm /var/lib/pacman/db.lck

# clear sessions cache
rm -rf ~/.cache/sessions/*

# clear full cache
# rm -rf ~/.cache

#clear aur temp directory
#rm -rf .tempaur/*

# flush dns
sudo nscd -i hosts

# flush font cache
fc-cache -fv
fc-cache

# vacuum journalctl
sudo journalctl --vacuum-size=500M && sudo journalctl --vacuum-time=2d
